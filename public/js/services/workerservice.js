angular.module('WorkerService', [])
    .factory('WebWorker', ['$q',function($q){

      var defer = $q.defer();
      var service = {};

      var myWorker = new Worker("worker.js");
      myWorker.addEventListener('message', function(e){
        console.log('Worker said: ', e.data);
        defer.resolve(e.data);
      }, false);

      service.syncLocalTaskToRemoteDb =  function () {
            var data = [{me:'All of it'}, {me:'All of it 2'}];
               defer2 = $q.defer();
               myWorker.postMessage(data);
               return defer2.promise;
      };

       //console.log('WorkerService');
        return service;
    }]);
