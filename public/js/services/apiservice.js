angular.module('ApiService', []).factory('API', ['$http','$q', function($http,$q) {
  //console.log('Api loaded');
  if (!window.indexedDB) {
    console.log('No indexedDB');
    window.alert("Your browser doesn't support a stable version of IndexedDB. Some fetures will not be available.");
  }


  var databaseName = 'task_database';
  //var req = indexedDB.deleteDatabase(databaseName);
  //var deferred = $q.defer();

  var service = {};

  service.getIndexedDb = function () {
    var db = new Dexie("task_database");
    db.version(1).stores({ tasks: 'id,task,created_by,sync' });

    return db;

    /*
    if (db) {
    deferred.resolve(db);
  } else {
  deferred.reject(e);
}
return deferred.promise
*/
};

function postSuccessCallback(results) {
  //console.log(results);
  return results;
}

function getSuccessCallback(results) {
  //console.log(results);
  return results;
}

function errorCallback(error) {
  return error;
}

service.postCreateTask = function (data) {
  return $http({
    method: 'POST',
    url: '/api/task/create', data
  }).then(postSuccessCallback,errorCallback)

};

service.getAllTask = function (data) {
  return $http({
    method: 'GET',
    url: '/api/task',
  }).then(getSuccessCallback,errorCallback)


};


return service;


}]);
