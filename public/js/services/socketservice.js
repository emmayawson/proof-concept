angular.module('SocketService', []).factory('socket', ['$http', '$q', '$rootScope',function ($http, $q, $rootScope) {

  //console.log('Socket service up ...');
  var service = {};
  var url = location.origin
  var socket = io.connect(url);

  service.on = function (eventName, callback) {
    socket.on(eventName, function () {
      var args = arguments;
      $rootScope.$apply(function () {
        callback.apply(socket, args);
      });
    });
  };

  service.emit =  function (eventName, data, callback) {
    socket.emit(eventName, data, function () {
      var args = arguments;
      $rootScope.$apply(function () {
        if (callback) {
          callback.apply(socket, args);
        }
      });
    })
  };


  return service;
}]);
