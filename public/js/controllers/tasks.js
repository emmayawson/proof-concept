angular.module('TasksCtrl', []).controller('TaskController', function($scope, $timeout, $document,$window, toaster,ngToast,API,socket) {
    $scope.tagline = 'To the moon and back vodes!';

    var db = API.getDb();

    $scope.todo = {};

  //  var socket = io.connect('ws://localhost:5000');

  $scope.processVote = function (todo) {
    console.log('Processin votes...');
    var uuid = Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
     console.log('UUID', uuid);
    db.tasks.put({ task: todo.name, sync: false}).then (function(){
              //
              // Then when data is stored, read from it
              //
      console.log('Saved!');
      $window.location.assign('/');
      // $location.path('/');
      // $location.replace();
          }).catch(function(error) {
             //
             // Finally don't forget to catch any error
             // that could have happened anywhere in the
             // code blocks above.
             //
             console.log("Ooops: " + error);
          });

  };
  //$scope.allTasks = [];
  $scope.msg = '';


  socket.on('task:posted',function taskPosted(data) {
    $timeout($scope.bindModel);

    //get all tasks
    var allTasks = API.getAllTask();
    allTasks.then(successHandler).catch(errorHandler);
     console.log(data);
     $rootScope.taskItems.push(data)
  });

  function postSuccessHandler(results) {
    $timeout($scope.bindModel);

    ngToast.warning({ content: 'Task saved' });
    $window.location.assign('/');


    return toaster.pop('success', 'Task saved', results.data.msg );
  }

   function successHandler() {

  }

   function errorHandler() {
    $timeout($scope.bindModel);
    $scope.msg = results.data.msg;
    ngToast.warning({content: $scope.msg});

    return toaster.pop('error' ,'Error ', $scope.msg );
  }






  $scope.saveTask = function (todo) {
    //console.log(todo);
    //$scope.processVote(todo);
    var taskRes = API.postCreateTask(todo);
    taskRes.then(postSuccessHandler)
    .catch(errorHandler);
  }


});
