angular.module('MainCtrl', []).controller('MainController', function($scope, $timeout, $document, $interval, $rootScope ,ngToast,toaster,API, socket, $firebaseObject) {

  $scope.taskItems = [];
  $scope.todo = {};

  // UUID func
  function guid() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
  }


  var DB = API.getIndexedDb();
  if($rootScope.online){
    //get initial data from firebase
    firebase.database().ref('tasks/').once('value').then(function(snapshot) {
      $scope.taskItems = snapshot.val();
      $timeout($scope.bindModel);
    });
  }else{
    //Get initial data from indexedDb
    var tasksRes = DB.tasks.toArray();
    tasksRes.then(successHandler)
    .catch(errorHandler);

  }



  //On change of values is firebase db,  diplsy new data
  var ref = firebase.database().ref('tasks/');
  ref.on("value", function(snapshot) {
    // This isn't going to show up in the DOM immediately, because
    // Angular does not know we have changed this in memory.
    // To fix this, we can use $scope.$apply() to notify Angular that a change occurred.
    //$scope.$apply(function() {
    $timeout(function(){
      $scope.taskItems = snapshot.val();
      //console.log($scope.taskItems);
    });

  });


//Delete task
$scope.deleteTask  = function (el){
var idToDelete = el.target.attributes.data.value;

  firebase.database().ref('tasks/id'+ idToDelete).remove();
};

  //Save a new task to firebase
  function writeToDb(id, task, username) {
    firebase.database().ref('tasks/id' + id).set({
      id: id,
      created_by: username,
      task: task,
    });

    //Clear input fields
    $scope.todo = {};
    return   ngToast.success({ content: 'Task saved' });
  }



  //Sync users local tasks  to firebase on network recovery
  function syncToDb(id, task, username) {

    if (id  !== undefined && task !== undefined  && username !== undefined){
      firebase.database().ref('tasks/id' + id).set({
        id: id,
        created_by: username,
        task: task,
      }, function (error) {
        if (error) {
          console.log(error);
        } else {
          //Delete the item from the client
          DB.tasks.delete(id);
        }
      });


      return   ngToast.success({ content: 'Local task synced successfully' });
    }

  }


  //get all tasks
  //var allTasks = API.getAllTask();
  //allTasks.then(successHandler).catch(errorHandler);

  //Switch db based on network status
  $scope.$watch('online', function(networkStatus) {
    console.log('Online : ',networkStatus);

    if(networkStatus){
      //get initial data from firebase
      firebase.database().ref('tasks/').once('value').then(function(snapshot) {
        $scope.taskItems = snapshot.val();
        $timeout($scope.bindModel);
      });

    }else{

      var tasksRes = DB.tasks.toArray();
      tasksRes.then(successHandler)
      .catch(errorHandler);

    }

  });



      var syncProcessor = function  () {

        return DB.tasks.where('sync')
        .equals('false')
        .limit(1)
        .toArray()
        .then(function (tasks) {

          if (tasks[0] !== undefined ){
            var task = tasks[0];

            //Sync to firebase
            syncToDb(task.id, task.task, task.created_by);
          } else {
            console.log('No tasks  in local to sync');
          }

        }).catch(function(err){
          console.log(err.message);
        });
      };



  //Start web workers
  $scope.$watch('online', function(networkStatus) {
    if(!window.Worker) {
      return ngToast.error({ content: 'You browser does not suppor web workers' });
    }

    if (networkStatus === true) {

        // This is done on each reload untill there are no tasks
        syncProcessor();

      //get total number of tasks in IndexedDb
      var tasksCount = DB.tasks.count();
      tasksCount.then(function (count) {

        console.log(count);

        /*
        it blocks the UI
        var i = 1;

        do {
        syncProcessor();
      } while (i < count);

      */

    }).catch(function (err) {
      console.log(err.message);
    });

    //this funciton wraps the code that saves the local data
    // to a remote server



  }
});

var indexedDBWorker = function  () {
  //require  worker script
  //var myWorker = new Worker("worker.js");
  //console.log(myWorker);

};

//Get socket update task
socket.on('task:posted',function taskPosted(data) {
  $timeout($scope.bindModel);

  //Get all tasks from local sqlite database
  //var allTasks = API.getAllTask();
  //allTasks.then(successHandler).catch(errorHandler);

  //TODO move  this into a directive
  //Display on screen last inserted task
  $(function(){
    var ui = '<a href="#" class="list-group-item"  id="'+ data.id+'"> ';
    ui += '<h4 class="list-group-item-heading">  '+ data.task + ' </h4>';
    ui += '<p class="list-group-item-text">Created by '+ data.name +'</p> ';
    ui += '</a>';
    var holder =  $('#task-holder');
    holder.prepend(ui);

  });
  //$scope.taskItems.push(data)
  //allTasks.then(successHandler).catch(errorHandler);
});

$scope.$watchCollection('fbdata ', function() {
  //console.log('taskItems changed');

});

function postSuccessHandler(results) {
  $timeout($scope.bindModel);
  return ngToast.success({ content: 'Task saved' });
  //$window.location.assign('/');
}


function saveTaskOffline (todo) {
  console.log('Processin offline...');
  var db = API.getIndexedDb();
  db.tasks.put({ id: guid(),task: todo.task, created_by: todo.name,sync: 'false'}).then (function(){

    console.log('Saved to index db!');

    //Clear input fields
    $scope.todo = {};

    var tasksRes = DB.tasks.toArray();
    tasksRes.then(successHandler)
    .catch(errorHandler);

  }).catch(function(error) {

    console.log(error.message);
  });

}

//save new task
$scope.saveTask = function (todo) {

  //Save data to indexedDB if there is no network
  if($rootScope.online){
    //save to firebase o sqlite db on local machine if true
    writeToDb(guid(), todo.task, todo.name);
  }else{
    //save to indexed db if false
    saveTaskOffline(todo);
  }

  /*
  //Save to SQLite db
  var taskRes = API.postCreateTask(todo);
  taskRes.then(postSuccessHandler)
  .catch(errorHandler);
  */
};


function successHandler(response) {
  // It nofifies the scope of data change. If you dont do this the data will not render on the page
  //Its wrapped in a timeout so that the $scope.apply is run
  $timeout($scope.bindModel);
  $timeout(function(){
    //$scope.taskItems = response.data;
    $scope.taskItems = response;

  });

}

function errorHandler(error) {
  console.log(error);
}



});
