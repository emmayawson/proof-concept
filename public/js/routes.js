// public/js/appRoutes.js
angular.module('appRoutes', [])
.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {

  $routeProvider
  // home page
  .when('/', {
    templateUrl: 'views/home.html',
    controller: 'MainController'
  })
  .when('/task/create', {
    templateUrl: 'views/task/create.html',
    controller: 'TaskController'
  });

  $locationProvider.html5Mode({
    enabled: true
  });

}]);
