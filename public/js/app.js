
angular.module('demoApp', ['ngRoute', 'ngSanitize','appRoutes', 'MainCtrl', 'TasksCtrl','ngAnimate', 'toaster' ,'ApiService', 'ngToast', 'SocketService','firebase', 'WorkerService'])
.config(function () {

})
.run(function($window, $rootScope) {
  $rootScope.online = navigator.onLine;
  $window.addEventListener("offline", function() {
    $rootScope.$apply(function () {
      console.log('OFFLINE');
      $rootScope.online = false;
    });
  }, false);

  $window.addEventListener("online", function() {
    $rootScope.$apply(function () {
      console.log('ONLINE');
      $rootScope.online = true;
    });
  }, false);



});
