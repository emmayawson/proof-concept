/*jslint node: true */
'use strict';

var express = require('express');
var app            = express();
var bodyParser     = require('body-parser');
var methodOverride = require('method-override');
var server = require('http').Server(app);
var io = require('socket.io')(server);
var path = require('path');
var sqlite3 = require('sqlite3').verbose();
var dbPath = path.resolve(__dirname, 'taskdb.db');
var db = new sqlite3.Database(dbPath);


//Set up of libs
//Socket.io conecction
    io.on('connection', function(socket){
       console.log('A Client has connected to the web socket');
        //socket.emit("message",["Hi", "There"]);
    });
//console.log(db);

/*
db.serialize(function() {
    db.run("CREATE TABLE IF NOT EXISTS tasks (id INTEGER PRIMARY KEY ,task TEXT, created_by TEXT)");
    db.run("INSERT INTO tasks (task, created_by) VALUES (?, ?)", "First default task", "Emmanuel");
});

*/


//set vars
var enviroment = process.env.APP_ENV || 'undefined';
var port = process.env.PORT || 5000; //Set default port

// get all data/stuff of the body (POST) parameters
// parse application/json
app.use(bodyParser.json());

// parse application/vnd.api+json as json
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// override with the X-HTTP-Method-Override header in the request. simulate DELETE/PUT
app.use(methodOverride('X-HTTP-Method-Override'));

// set the static files location /public/img will be /img for users
app.use(express.static(__dirname + '/public'));



app.post('/api/task/create', function (req, res) {
   console.log(req.body);
   db.run("INSERT INTO tasks (task, created_by) VALUES (?, ?)", req.body.task, req.body.name, function (err) {
     if (err) {
       console.log(err);
       return res.json({msg: 'Error, your task could not be saved!'});
     }
     io.emit('task:posted',req.body);
     return res.json(req.body);
    });

});

 app.get('/api/task', function (req, res) {

    db.all("SELECT * FROM tasks", function (err, row) {
     if (err) {
       return res.json({msg: 'Error, cannot retrieve your tasks now!'});
     }
      console.log(row);
     return res.send(row.reverse());
    });

 });

//catch all routes and serve the index.html page
//Any route placed after this will not be executed
app.get('*', function(req, res) {
  res.sendFile(path.resolve(__dirname, './public/views/index.html')); // load our public/index.html file
 });

//start server
server.listen(port, function(){
    console.log('Listening on port ' + port);
    //Auto refresh browser on reload
    if (process.send) {
        process.send('online');
    }

});
